package Observable;

import Observer.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FearAndGreedIndex implements Observable{

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void attach(Observer observer)
    {
        observers.add(observer);
    }
    @Override
    public void detach(Observer observer)
    {
        observers.remove(observer);
    }
    @Override
    public void notifyObservers(String value_classification, int value)
    {
        for(Observer observer:observers)
        {
            observer.update(value_classification,value);
        }
    }

    public JSONObject httpRequest() {
        String link = "https://api.alternative.me/fng/";
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(link))
                    .timeout(Duration.ofSeconds(5))
                    .GET()
                    .build();
            HttpClient client = HttpClient.newBuilder().build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return ((JSONObject) JSONValue.parse(response.body()));

        } catch (Exception e) {
            return new JSONObject();
        }
    }

    public String FearAndGreedValueClassification()
    {
        if(!Objects.equals(httpRequest(), new JSONObject()))
        {
            JSONArray o1 =(JSONArray) httpRequest().get("data");
            JSONObject o2 = (JSONObject) o1.get(0);
//            System.out.println(o2.get("value_classification"));
            return (String) o2.get("value_classification");
        }
        else
            return "";
    }

    public Integer FearAndGreedValue()
    {
        if(!Objects.equals(httpRequest(), new JSONObject()))
        {
            JSONArray o1 =(JSONArray) httpRequest().get("data");
            JSONObject o2 = (JSONObject) o1.get(0);
//            System.out.println(o2.get("value"));
            String o3 = (String) o2.get("value");
            return Integer.parseInt(o3);
        }
        else
            return -10;
    }

}
