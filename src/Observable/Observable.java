package Observable;
import Observer.Observer;
public interface Observable {
    void attach(Observer observer);
    void detach(Observer observer);
    void notifyObservers(String value_classification, int value);

}
