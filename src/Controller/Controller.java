package Controller;

import Model_Strategy.CoinPaprika;
import Model_Strategy.Coingecko;
import Model_Strategy.CryptoCompare;
import app.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Controller {


    private Parameters pa = new Parameters();
    private String global_currency_name;

    @FXML
    public LineChart<String, Number> lineChart;
    @FXML
    public NumberAxis y;
    @FXML
    public CategoryAxis x;


    @FXML
    private Text crypto_quantity, error;

    @FXML
    private TextField number_of_crypto;

    @FXML
    private MenuButton crypto_menubutton, api_menubutton, currency_menubutton;

    public void switchToChart(ActionEvent event) throws IOException {
        save();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/Chart.fxml"));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
        ChartController charts = loader.getController();
        charts.initData(pa);
        stage.show();

    }



    public void initialize()
    {
        error_message("");
        number_of_crypto.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,-]", ""));
            return change;
        }));
        try {
            FileInputStream fi = new FileInputStream(new File("Data/last_settings.data"));
            ObjectInputStream oi = new ObjectInputStream(fi);
            pa = (Parameters) oi.readObject();
            BigDecimal crypto_quantily= BigDecimal.valueOf(pa.getCrypto_quantity());
            number_of_crypto.setText(crypto_quantily.toPlainString());
            error_message("");
            which_api(pa.getApi_name());
            which_currency(pa.getCurrency_name());
            which_crypto(pa.getCrypto_name());
            pa.setError_messages("");
        }
        catch (Exception ee) {
            number_of_crypto.setText("1.11");
            error_message("");
            which_api("CoinPaprika");
            which_currency("USD");
            which_crypto("BTC");
        }

    }

    public void save()
    {
        try {
            pa.setCrypto_quantity(Double.parseDouble(number_of_crypto.getText().replace(",",".")));
            FileOutputStream fileOut = new FileOutputStream("Data/last_settings.data");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(pa);
            objectOut.close();

        }
        catch (Exception e)
        {
            System.out.println("Can't save");
            System.out.println(e);
        }
    }
    public void which_crypto(String crypro_name)
    {
        Label t = new Label(crypro_name);
        crypto_menubutton.setGraphic(t);
        pa.setCrypto_name(crypro_name);

    }

    public void which_api(String api_name)
    {
        Label t = new Label(api_name);
        api_menubutton.setGraphic(t);
        pa.setApi_name(api_name);
    }

    public void which_currency(String currency_name)
    {
        Label t = new Label(currency_name);
        currency_menubutton.setGraphic(t);
        pa.setCurrency_name(currency_name);
        global_currency_name = currency_name;
    }

    public void error_message(String error_message){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        if(error_message!="")
            error_message+= "-" + LocalTime.now().format(formatter);
        error.setText(error_message);
    }

    public void btc_menuitem_onclick(ActionEvent e){
        which_crypto("BTC");
    }

    public void usdt_menuitem_onclick(ActionEvent e){
        which_crypto("USDT");
    }

    public void ada_menuitem_onclick(ActionEvent e){
        which_crypto("ADA");
    }

    public void eth_menuitem_onclick(ActionEvent e){
        which_crypto("ETH");
    }

    public void coinpaprica_onclick(ActionEvent e){
        which_api("CoinPaprika");
    }

    public void cryptocompare_onclick(ActionEvent e){
        which_api("CryptoCompare");
    }

    public void coingecko_onclick(ActionEvent e){
        which_api("Coingecko");
    }

    public void usd_onclick(ActionEvent e)
    {
        which_currency("USD");
    }

    public void gbp_onclick(ActionEvent e)
    {
        which_currency("GBP");
    }

    public void pln_onclick(ActionEvent e)
    {
        which_currency("PLN");
    }

    public void eur_onclick(ActionEvent e)
    {
        which_currency("EUR");
    }

    public void subbmit(ActionEvent e){

        error_message("");
        try {
            pa.setCrypto_quantity(Double.parseDouble(number_of_crypto.getText().replace(",",".")));
            if(pa.getCrypto_quantity()==0)
            {
                throw new Exception("");
            }
        }
        catch (Exception ee)
        {
            error_message("Błąd przy wpisywaniu liczby.");
            return;
        }

        switch (pa.getApi_name()) {
            case "CoinPaprika" -> {
                CoinPaprika coinpaprika = new CoinPaprika(pa);
                coinpaprika.Converter();
            }
            case "Coingecko" -> {
                Coingecko coingecko = new Coingecko(pa);
                coingecko.Converter();
            }
            case "CryptoCompare" -> {
                CryptoCompare cryptocompare = new CryptoCompare(pa);
                cryptocompare.Converter();
            }
            default -> {
                error_message("Błąd w wybraniu API");
                return;
            }
        }
        if(Objects.equals(pa.getError_messages(), "")) {
            int output_price = (int) (pa.getCrypto_price()*100);

            crypto_quantity.setText((double)output_price/100 +" "+global_currency_name);
            save();
        }
        error_message(pa.getError_messages());
    }

    public void wallet_onclick(ActionEvent e) throws IOException {
        save();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/Wallet.fxml"));
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
        stage.show();
    }

    public void buy_onclick(ActionEvent e) {
        error_message("");
        subbmit(e);
        if(Objects.equals(pa.getError_messages(), "")) {
            Wallet wallet = new Wallet();
            error_message(wallet.buy(pa,crypto_quantity.getText()));
        }

    }

    public void sell_onclick(ActionEvent e)
    {
        error_message("");
        subbmit(e);
        if(Objects.equals(pa.getError_messages(), "")) {
            Wallet wallet = new Wallet();
            error_message(wallet.sell(pa,crypto_quantity.getText()));
        }
    }


}
