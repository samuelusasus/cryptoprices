package Controller;

import app.Wallet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public class WalletController {

    @FXML
    private Text btc,usdt,eth,ada,pln,usd,eur,gbp;

    public  String format(Double a,int precision)
    {
        BigDecimal b = new BigDecimal(a);
        String temp = b.toPlainString();
        if(!temp.contains("."))
        {
            temp+=".";
        }
        for (int i=0;i<precision;i++)
        {
            temp+="0";
        }
//        System.out.println(a);
        return temp.substring(0,temp.indexOf(".")+precision+1);
    }

    public void initialize()
    {
        Wallet wallet = new Wallet();
        Map<String,Double> map = wallet.getMap();
        btc.setText("BTC:"+ format(map.get("BTC"),5));
        usdt.setText("USDT:"+format(map.get("USDT"),5));
        eth.setText("ETH:"+format(map.get("ETH"),5));
        ada.setText("ADA:"+format(map.get("ADA"),5));
        pln.setText("PLN:"+format(map.get("PLN"),2));
        usd.setText("USD:"+format(map.get("USD"),2));
        gbp.setText("GBP:"+format(map.get("GBP"),2));
        eur.setText("EUR:"+format(map.get("EUR"),2));
    }

    public void switchToMain(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/Main.fxml"));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
        stage.show();
    }
}
