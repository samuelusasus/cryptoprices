package Controller;

import Model_Strategy.Charts;
import app.Parameters;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.stage.Stage;

import java.io.IOException;

public class ChartController {
    @FXML
    public LineChart <String,Number> lineChart;
    @FXML
    public NumberAxis y;
    @FXML
    public CategoryAxis x;

    Parameters pa;

    public void initData(Parameters pa)
    {
        this.pa = pa;
        Charts charts = new Charts(pa);
        charts.Converter();
        if(!pa.getError_messages().contains("Błąd w komunikacji z API"))
        {
            y.setForceZeroInRange(false);
            x.setAnimated(false);
            y.setAnimated(false);
            lineChart.setTitle("Wykres "+pa.getCrypto_name()+"/"+pa.getCurrency_name()+" w ostatnich 24h");
            lineChart.getData().clear();
            lineChart.getData().add(pa.getSeries());
        }
        else
        {
            lineChart.setTitle("Błąd w komunikacji z API");
        }
    }

    public void switchToMain(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/Main.fxml"));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
        stage.show();
    }
}
