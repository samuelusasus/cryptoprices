package Observer;

public interface Observer {
    void update(String value_classification, int value);
}
