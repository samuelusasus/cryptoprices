package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

public class Main extends Application{
    CheckFearAndGreed checkFearAndGreed = new CheckFearAndGreed();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/View/Main.fxml")));
        primaryStage.setTitle("CryptoPrices");
        Scene scene=new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        new Thread(checkFearAndGreed::watch).start();
    }
    @Override
    public void stop(){
        checkFearAndGreed.setEnd_fear_and_greed_thread(true);
    }
    public static void main(String[] args) {
        launch(args);
    }
}


