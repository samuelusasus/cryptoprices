package app;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Wallet implements Serializable{
    Map<String,Double> map = new HashMap<>();

    public Wallet()
    {
        import_data();
    }

    public boolean can_i_sell(Parameters pa)
    {
//        System.out.println("sell");
//        System.out.println(map.get(pa.getCrypto_name()));
//        System.out.println(pa.getCrypto_quantity());
        if(map.get(pa.getCrypto_name())<pa.getCrypto_quantity())
        {
            return false;
        }
        return true;
    }
    public boolean can_i_buy(String fiat)
    {
        double fiat_number = Double.parseDouble(fiat.substring(0,fiat.length()-4));
        String currency = fiat.substring(fiat.length()-3);
        if(map.get(currency)<fiat_number)
        {
            return false;
        }
        else
            return true;
    }
    public String buy(Parameters pa, String fiat)
    {
        double fiat_number = Double.parseDouble(fiat.substring(0,fiat.length()-4));
        if(can_i_buy(fiat)){
            String currency = fiat.substring(fiat.length()-3);
//            System.out.println(fiat_number);
//            System.out.println(pa.getCrypto_quantity());
            map.put(currency,map.get(currency)-fiat_number);
            map.put(pa.getCrypto_name(), map.get(pa.getCrypto_name()) + pa.getCrypto_quantity());
            this.save_data();
            return "Dokonano transakcji";
        }
        else
        {
            return "Tranzakcja niemozliwa do zrealizowania";
        }
    }
    public String sell(Parameters pa, String fiat)
    {
        double fiat_number = Double.parseDouble(fiat.substring(0,fiat.length()-4));
        if(can_i_sell(pa)){
            String currency = fiat.substring(fiat.length()-3);
            map.put(currency,map.get(currency)+fiat_number);
            map.put(pa.getCrypto_name(), map.get(pa.getCrypto_name()) - pa.getCrypto_quantity());
            this.save_data();
            return "Dokonano transakcji";
        }
        else
        {
            return "Tranzakcja niemozliwa do zrealizowania";
        }
    }
    public void import_data()
    {
        try {
            FileInputStream fi = new FileInputStream(new File("Data/wallet.data"));
            ObjectInputStream oi = new ObjectInputStream(fi);
            map = (Map<String, Double>) oi.readObject();
            fi.close();
            oi.close();
        }
        catch (Exception ee) {
            map.put("BTC",0.0);
            map.put("USDT",0.0);
            map.put("ADA",0.0);
            map.put("ETH",0.0);
            map.put("PLN",100.0);
            map.put("USD",100.0);
            map.put("EUR",100.0);
            map.put("GBP",100.0);
        }
    }
    public void save_data(){
        try {
            FileOutputStream fileOut = new FileOutputStream("Data/wallet.data");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(map);
            objectOut.close();
            fileOut.close();

        }
        catch (Exception e)
        {
            System.out.println("Can't save");
            System.out.println(e);
        }
    }

    public Map<String, Double> getMap() {
        return map;
    }

    public void setMap(Map<String, Double> map) {
        this.map = map;
    }
}
