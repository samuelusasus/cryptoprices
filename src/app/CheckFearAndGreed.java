package app;

import Observable.FearAndGreedIndex;
import Observer.FearAndGreadAllert;

import java.util.concurrent.TimeUnit;

public class CheckFearAndGreed {
    private FearAndGreedIndex fearAndGreedIndex = new FearAndGreedIndex();
    private FearAndGreadAllert fearAndGreadAllert = new FearAndGreadAllert();

    public boolean isEnd_fear_and_greed_thread() {
        return end_fear_and_greed_thread;
    }

    public void setEnd_fear_and_greed_thread(boolean end_fear_and_greed_thread) {
        this.end_fear_and_greed_thread = end_fear_and_greed_thread;
    }

    private boolean end_fear_and_greed_thread = false;

    public void watch(){
        long counter=0;
        int old_value=-10;
        fearAndGreedIndex.attach(fearAndGreadAllert);
        while (!end_fear_and_greed_thread)
        {
            if(counter%10==0)
            {
                counter = 0;
                int value = fearAndGreedIndex.FearAndGreedValue();
                String valueclassification = fearAndGreedIndex.FearAndGreedValueClassification();

                switch (valueclassification)
                {
                    case "Extreme Fear":
                        valueclassification = "Ogromny strach";
                        break;
                    case "Fear":
                        valueclassification = "Strach";
                        break;
                    case "Neutral":
                        valueclassification = "Neutralność";
                        break;
                    case "Greed":
                        valueclassification = "Chciwość";
                        break;
                    case "Extreme Greed":
                        valueclassification = "Ogromna Chciwość";
                        break;
                }
                if(value!=old_value && !valueclassification.equals("") && value!=-10)
                {
                    fearAndGreedIndex.notifyObservers(valueclassification,value);
                    old_value = value;
                }

            }
            counter++;
            try {
                TimeUnit.SECONDS.sleep(1);
            }
            catch (Exception ignored) {}

        }

    }
}
