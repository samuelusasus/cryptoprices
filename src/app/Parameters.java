package app;

import javafx.scene.chart.XYChart;

import java.io.Serializable;

public class Parameters implements Serializable {
    private String crypto_name;
    private double crypto_quantity;
    private double crypto_price;
    private String api_name;
    private String currency_name;
    private String error_messages;
    private transient XYChart.Series<String,Number> series;

    public Parameters()
    {
        crypto_name = "BTC";
        crypto_quantity = 1;
        crypto_price=1;
        api_name = "CoinPaprikaa";
        currency_name = "USD";
        error_messages = "";
        series = new XYChart.Series<>();
    }


    public String getCrypto_name()                         {return crypto_name;}
    public void setCrypto_name(String crypto_name)         {this.crypto_name = crypto_name;}
    public double getCrypto_quantity()                     {return crypto_quantity;}
    public void setCrypto_quantity(double crypto_quantity) {this.crypto_quantity = crypto_quantity;}
    public double getCrypto_price()                        {return crypto_price;}
    public void setCrypto_price(double crypto_price)       {this.crypto_price = crypto_price;}
    public String getApi_name()                            {return api_name;}
    public void setApi_name(String api_name)               {this.api_name = api_name;}
    public String getCurrency_name()                       {return currency_name;}
    public void setCurrency_name(String currency_name)     {this.currency_name = currency_name;}
    public String getError_messages()                      {return error_messages;}
    public void setError_messages(String error_messages)   {this.error_messages = error_messages;}
    public XYChart.Series<String, Number> getSeries() {
        return series;
    }
    public void setSeries(XYChart.Series<String, Number> series) {
        this.series = series;
    }
}
