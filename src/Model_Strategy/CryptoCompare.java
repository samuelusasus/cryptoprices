package Model_Strategy;

import app.Parameters;
import org.json.simple.JSONObject;

public class CryptoCompare extends CryptomarketAPI{


    public CryptoCompare(Parameters pa){
        super(pa);
    }

    @Override
    public void converter(){
        Parameters parameters = getParameters();
        String api_key = "1d8aff8b2475d32249a72ac3ac075bbfeec25344bfcea1008b60f117cda3bff1";

        String link="https://min-api.cryptocompare.com/data/price?";
        link += "fsym="+parameters.getCrypto_name();
        link += "&tsyms="+parameters.getCurrency_name();
        link += "&api_key="+api_key;
        JSONObject o1 = httpRequest(link);
        try {
//            System.out.println("CryptoCompare:"+o1.get(parameters.getCurrency_name()));
            parameters.setCrypto_price(Double.parseDouble(o1.get(parameters.getCurrency_name()).toString())*parameters.getCrypto_quantity());
        }
        catch (Exception e){/**/}


    }

}
