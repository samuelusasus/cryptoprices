package Model_Strategy;

import app.Parameters;
import org.json.simple.JSONObject;

import java.util.Locale;

public class CoinPaprika extends CryptomarketAPI {

    public CoinPaprika(Parameters pa){
        super(pa);
    }

    @Override
    public void converter(){
        Parameters parameters = getParameters();
        String crypto_id;
//        System.out.println(parameters.getCrypto_name());
        crypto_id = switch (parameters.getCrypto_name()) {
            case "BTC" -> "btc-bitcoin";
            case "ADA" -> "ada-cardano";
            case "USDT" -> "usdt-tether";
            case "ETH" -> "eth-ethereum";
            default -> "btc-bitcoin";
        };
//        System.out.println(crypto_id);

        String link="https://api.coinpaprika.com/v1/price-converter?";
        link += "base_currency_id="+crypto_id;
        link += "&quote_currency_id=usd-us-dollars";
        link += "&amount="+parameters.getCrypto_quantity();
        JSONObject o1 = httpRequest(link);
//        System.out.println((Double) o1.get("price"));
        String link2 = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/usd/" + parameters.getCurrency_name().toLowerCase(Locale.ROOT) + ".json";
        JSONObject o2 = httpRequest(link2);
        try{
//            System.out.println(o2.get(parameters.getCurrency_name().toLowerCase(Locale.ROOT)));
            parameters.setCrypto_price((Double) o1.get("price") * Double.parseDouble(o2.get(parameters.getCurrency_name().toLowerCase(Locale.ROOT)).toString()));

        }
        catch (Exception e)
        {
            //
        }


    }
}
