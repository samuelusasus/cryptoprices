package Model_Strategy;

import app.Parameters;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public abstract class CryptomarketAPI {
    private Parameters parameters;

    public Parameters getParameters()               {return parameters;}
    public void setParameters(Parameters parameters){this.parameters = parameters;}

    public CryptomarketAPI(Parameters pa){
        this.parameters = pa;
    }

    public JSONObject httpRequest(String link){
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(link))
                    .timeout(Duration.ofSeconds(5))
                    .GET()
                    .build();
            HttpClient client = HttpClient.newBuilder().build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return ((JSONObject) JSONValue.parse(response.body()));

        }
        catch (Exception e) {
            if(!this.getParameters().getError_messages().contains("Błąd w komunikacji z API"))
                this.getParameters().setError_messages(getParameters().getError_messages() + "Błąd w komunikacji z API");
            System.out.println(e);
            return new JSONObject();
        }
    }

    public abstract void converter();

    public void Converter(){
        this.converter();
    }

}
