package Model_Strategy;

import app.Parameters;
import javafx.scene.chart.XYChart;
import org.json.simple.JSONArray;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;

public class Charts extends CryptomarketAPI{
    public Charts(Parameters pa)
    {
        super(pa);
    }
    @Override
    public void converter(){
        Parameters parameters = getParameters();
        String crypto_id;

        crypto_id = switch (parameters.getCrypto_name()) {
            case "BTC" -> "bitcoin";
            case "ADA" -> "cardano";
            case "USDT" -> "tether";
            case "ETH" -> "ethereum";
            default -> "bitcoin";
        };

        String link="https://api.coingecko.com/api/v3/coins/";
        link += crypto_id;
        link += "/market_chart?vs_currency="+parameters.getCurrency_name();
        link +="&days=1&interval=hourly";
        try
        {
            JSONArray o1 =(JSONArray) httpRequest(link).get("prices");
            XYChart.Series<String,Number> series = new XYChart.Series<>();
            for (Object o : o1) {
                JSONArray temp = (JSONArray) o;
                String date = (LocalDateTime.ofInstant(Instant.ofEpochMilli((Long) temp.get(0)),
                        TimeZone.getDefault().toZoneId()).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME));
                series.getData().add(new XYChart.Data<>(date, (Number) temp.get(1)));
            }
            series.setName(parameters.getCrypto_name()+"/"+parameters.getCurrency_name());
            parameters.setSeries(series);
        }
        catch (Exception e)
        {
            if(!this.getParameters().getError_messages().contains("Błąd w komunikacji z API"))
                this.getParameters().setError_messages(getParameters().getError_messages() + "Błąd w komunikacji z API");
            System.out.println(e);
        }


    }
}
