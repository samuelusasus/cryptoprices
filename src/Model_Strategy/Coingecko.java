package Model_Strategy;

import app.Parameters;
import org.json.simple.JSONObject;
import java.lang.String;
import java.util.Locale;

public class Coingecko extends CryptomarketAPI{
    public Coingecko(Parameters pa){super(pa);}

    @Override
    public void converter(){
        Parameters parameters = getParameters();
        String crypto_id;

        crypto_id = switch (parameters.getCrypto_name()) {
            case "BTC" -> "bitcoin";
            case "ADA" -> "cardano";
            case "USDT" -> "tether";
            case "ETH" -> "ethereum";
            default -> "bitcoin";
        };

        String link="https://api.coingecko.com/api/v3/simple/price?";
        link += "ids="+crypto_id;
        link += "&vs_currencies="+parameters.getCurrency_name();
        JSONObject o1 =(JSONObject) httpRequest(link).get(crypto_id);
        try {
//            System.out.println(o1.get(parameters.getCurrency_name().toLowerCase(Locale.ROOT)).toString());
            parameters.setCrypto_price(Double.parseDouble(o1.get(parameters.getCurrency_name().toLowerCase(Locale.ROOT)).toString()) * parameters.getCrypto_quantity());
        }
        catch (Exception e){/**/}
    }
}
